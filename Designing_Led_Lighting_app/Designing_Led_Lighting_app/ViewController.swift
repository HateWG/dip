//
//  ViewController.swift
//  Designing_Led_Lighting_app
//
//  Created by Томас Димеджи Акинделе Ало on 27/02/2019.
//  Copyright © 2019 Томас Димеджи Акинделе Ало. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var ModelController: ModelConroller!
    
    @IBOutlet weak var roomLengthSlider: UISlider!
    @IBOutlet weak var roomWidthSlider: UISlider!
    @IBOutlet weak var roomHeightSlider: UISlider!
    
    @IBOutlet weak var lengthValue: UILabel!
    @IBOutlet weak var widhtValue: UILabel!
    @IBOutlet weak var heightValue: UILabel!
    
    
    @IBOutlet weak var luminaireOrientation: UISegmentedControl!
    
    
    @IBOutlet weak var requiredIllumination: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func changeTipRoomLength(_ sender: Any) {
        
        let lenghtRoundValue = roundf(roomLengthSlider.value) / 10
        lengthValue.text = lenghtRoundValue.description
        print("длинна помещения :\(lengthValue.text!)")
        
    }
    
    @IBAction func changeTipRoomWidth(_ sender: Any) {
        let widhtRoundValue = roundf(roomWidthSlider.value) / 10
        widhtValue.text = widhtRoundValue.description
        print("ширина помещения :\(widhtValue.text!)")
        
    }
    @IBAction func changeTipRoomHeight(_ sender: Any) {
        let heightRoundValue = roundf(roomHeightSlider.value) / 10
        heightValue.text = heightRoundValue.description
        print("высота помещения :\(widhtValue.text!)")
        
    }
    
    @IBAction func swithLuminaireOrintation(_ sender: Any) {
        
        print(luminaireOrientation.selectedSegmentIndex)
        if luminaireOrientation.selectedSegmentIndex == 0 {
            print("Ориентация светильников: Продольная")
        }else {
            print("Ориентация светильников: Поперечная")
        }
    }
    @IBAction func requiredIlluminationButtonAction(_ sender: Any) {
        
        
        let alertController = UIAlertController (title: nil, message: "choose", preferredStyle: .actionSheet)
        
        
        let action100 = UIAlertAction(title: "100", style: .default) { (action) in
            self.requiredIllumination.text = "100"
        }
        let action200 = UIAlertAction(title: "200", style: .default) { (action) in
            self.requiredIllumination.text = "200"
        }
        let action300 = UIAlertAction(title: "300", style: .default) { (action) in
            self.requiredIllumination.text = "300"
        }
        let action400 = UIAlertAction(title: "400", style: .default) { (action) in
            self.requiredIllumination.text = "400"
        }
        let action500 = UIAlertAction(title: "500", style: .default) { (action) in
            self.requiredIllumination.text = "500"
        }
        let action600 = UIAlertAction(title: "600", style: .default) { (action) in
            self.requiredIllumination.text = "600"
        }
        let action700 = UIAlertAction(title: "700", style: .default) { (action) in
            self.requiredIllumination.text = "700"
        }
        let action800 = UIAlertAction(title: "800", style: .default) { (action) in
            self.requiredIllumination.text = "100"
        }
        let action900 = UIAlertAction(title: "900", style: .default) { (action) in
            self.requiredIllumination.text = "900"
        }
        let action1000 = UIAlertAction(title: "1000", style: .default) { (action) in
            self.requiredIllumination.text = "1000"
        }
        let action1100 = UIAlertAction(title: "1100", style: .default) { (action) in
            self.requiredIllumination.text = "1100"
        }
        let action1200 = UIAlertAction(title: "1200", style: .default) { (action) in
            self.requiredIllumination.text = "1200"
        }
        let action1300 = UIAlertAction(title: "1300", style: .default) { (action) in
            self.requiredIllumination.text = "1300"
        }
        let action1400 = UIAlertAction(title: "1400", style: .default) { (action) in
            self.requiredIllumination.text = "1400"
        }
        let action1500 = UIAlertAction(title: "1500", style: .default) { (action) in
            self.requiredIllumination.text = "1500"
        }
        
        alertController.addAction(action100)
        alertController.addAction(action200)
        alertController.addAction(action300)
        alertController.addAction(action400)
        alertController.addAction(action500)
        alertController.addAction(action600)
        alertController.addAction(action700)
        alertController.addAction(action800)
        alertController.addAction(action900)
        alertController.addAction(action1000)
        alertController.addAction(action1100)
        alertController.addAction(action1200)
        alertController.addAction(action1300)
        alertController.addAction(action1400)
        alertController.addAction(action1500)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SendSegue" {
            if let controller = segue.destination as? SecondViewController {
                controller.ModelController = ModelController
                
                controller.length = (lengthValue.text! as NSString).doubleValue
                controller.width = (widhtValue.text! as NSString).doubleValue
                controller.height = (heightValue.text! as NSString).doubleValue
                
                controller.luminaireOrientation = luminaireOrientation.selectedSegmentIndex
                
                controller.requiredIllumination = Int((requiredIllumination.text! as NSString).intValue)
            }
            
        }
    }
    
    
}
