//
//  LampSelectionTableView.swift
//  Designing_Led_Lighting_app
//
//  Created by Томас Димеджи Акинделе Ало on 19/03/2019.
//  Copyright © 2019 Томас Димеджи Акинделе Ало. All rights reserved.
//

import UIKit

class LampSelectionTableView: UITableViewController {
    
    let cellId = "cell"
    
    var ModelController: ModelConroller!
    
    var lightOutputFrom: Double = 0
    var lightOutputTo: Double = 0
    
    var powerFrom: Double = 0
    var powerTo: Double = 0
    
    var firstDimensionValue: Int = 0
    var secondDimensionValue: Int = 0
    
    var dispersionValue: Int = 0
    
    var lightSourceColor: String = ""
    
    var seriasValue:String = ""
    
    struct lampConfiguration {
        var model: String
        
        var lightOutput: Int
        
        var power: Int
        
        var firstDimensionValue: Int
        var secondDimensionValue: Int
        
        var dispersionValue: Int
        
        var lightSourceColor: String
        
        var seriasValue:String

    }
    let configuration = [
         lampConfiguration(model: "qwe",
                           lightOutput: 3,
                           power: 4,
                           firstDimensionValue: 300, secondDimensionValue: 300,
                           dispersionValue: 4,
                           lightSourceColor: "123",
                           seriasValue: "123"
        )
    ]
        
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.dataSource = self
        tableView.delegate = self
        
        print("Световой поток от \(lightOutputFrom) до \(lightOutputTo)")
        print("Мощность от \(powerFrom) до \(powerTo)")
        
        switch dispersionValue {
        case 0:
            print("Тип рассеивателя: Не задано")
        case 1:
            print("Тип рассеивателя: Колотый лед")
        case 2:
            print("Тип рассеивателя: Молочный")
        case 3:
            print("Тип рассеивателя: Призматический")
        default:
            print("Error")
        }
        
        print("Цвет источника света: \(lightSourceColor)")
        print("Серия:\(seriasValue)")
        
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return configuration.count
    }
    
//    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 50
//    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! LampSelectionCell
        
        let lampModel = configuration[indexPath.row]
        cell.model = lampModel.model
        print(cell.model)
        print(lampModel.model)
        return cell
    }
    
}
