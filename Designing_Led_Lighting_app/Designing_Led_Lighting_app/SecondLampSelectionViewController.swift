//
//  SecondLampSelectionViewController.swift
//  Designing_Led_Lighting_app
//
//  Created by Томас Димеджи Акинделе Ало on 15/03/2019.
//  Copyright © 2019 Томас Димеджи Акинделе Ало. All rights reserved.
//

import UIKit

class SecondLampSelectionViewController: UIViewController {

    var ModelController: ModelConroller!
    
    var lightOutputFrom: Double = 0
    var lightOutputTo: Double = 0
    
    var powerFrom: Double = 0
    var powerTo: Double = 0
    
    var firstDimensionValue: Int = 0
    var secondDimensionValue: Int = 0
    
    var dispersionValue: Int = 0
    
    var lightSourceColor: String = ""
    
    var seriasValue:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("Световой поток от \(lightOutputFrom) до \(lightOutputTo)")
        print("Мощность от \(powerFrom) до \(powerTo)")
        
        switch dispersionValue {
        case 0:
            print("Тип рассеивателя: Не задано")
        case 1:
            print("Тип рассеивателя: Колотый лед")
        case 2:
            print("Тип рассеивателя: Молочный")
        case 3:
            print("Тип рассеивателя: Призматический")
        default:
            print("Error")
        }
        
        print("Цвет источника света: \(lightSourceColor)")
        print("Серия:\(seriasValue)")
    }

    
    
}
