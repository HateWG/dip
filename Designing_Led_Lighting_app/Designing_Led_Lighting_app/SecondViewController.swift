//
//  SecondViewController.swift
//  Designing_Led_Lighting_app
//
//  Created by Томас Димеджи Акинделе Ало on 13/03/2019.
//  Copyright © 2019 Томас Димеджи Акинделе Ало. All rights reserved.
//

import UIKit
import SpriteKit

class SecondViewController: UIViewController {

    var ModelController: ModelConroller!
    
    @IBOutlet weak var lengthVal: UILabel!
    @IBOutlet weak var widthVal: UILabel!
    @IBOutlet weak var heightVal: UILabel!
    @IBOutlet weak var luminaireOrientationVal: UILabel!
    @IBOutlet weak var requiredIlluminationVal: UILabel!
    
    
    
    @IBOutlet weak var spriteKitView: SKView!
//    var spriteKitScence: SKScene!
    
    var length: Double = 0.0
    var width: Double = 0.0
    var height: Double = 0.0
    var luminaireOrientation: Int = 0
    var requiredIllumination: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let viewWidht = view.frame.width
        let viewHeight = view.frame.height
        
        print(viewWidht)
        print(viewHeight)
//        print(SecondViewController.widh)
        print(length)
        print(width)
        print(height)
        print(luminaireOrientation)
        print(requiredIllumination)
        
        lengthVal.text = length.debugDescription
        widthVal.text = width.debugDescription
        heightVal.text = height.debugDescription
        if luminaireOrientation == 0 {
            luminaireOrientationVal.text = "Продольная"
        }else {
            luminaireOrientationVal.text =  "Поперечная"
        }
        requiredIlluminationVal.text = requiredIllumination.description
        

        let scene = SKScene(fileNamed: "MainScene")
        spriteKitView.presentScene(scene)
        
        SceneSetting(scene: scene!)
        
        SetSpriteKitViewSize(skView: spriteKitView, widhtMainView: Double(viewWidht), heightMainView: Double(viewHeight), roomWidht: width, roomHeight: length )
        roomModel(skView: spriteKitView)
    }
    
    func SceneSetting(scene: SKScene)
    {
        scene.backgroundColor = SKColor.orange
    }
    
    //        задаем размер комнаты
    func SetSpriteKitViewSize(skView: SKView, widhtMainView: Double, heightMainView: Double, roomWidht: Double, roomHeight: Double){
        var Wmultiplier: Double = 0
        var Hmultiplier: Double = 0
        var multiplier: Double = 0
        var iW: Double = 2
        var iH: Double = 2
        switch roomWidht {
        case 2...10:
            iW = roomWidht
            while iW != roomWidht {
                iW += 0.1
            }
            Wmultiplier = 300 / iW
        case 10.1...20:
            iW = roomWidht
            while iW != roomWidht {
                iW += 0.1
            }
            Wmultiplier = 300 / iW
        case 20.1...30:
            iW = roomWidht
            while iW != roomWidht {
                iW += 0.1
            }
            Wmultiplier = 300 / iW
        case 30.1...40:
            iW = roomWidht
            while iW != roomWidht {
                iW += 0.1
            }
            Wmultiplier = 300 / iW
        case 40.1...50:
            iW = roomWidht
            while iW != roomWidht {
                iW += 0.1
            }
            Wmultiplier = 300 / iW
        default:
            print("def")
        }
        
        
        switch roomHeight {
        case 2...10:
            iH = roomHeight
            while iH != roomHeight  {
                iH += 0.1
            }
            Hmultiplier = 300 / iH
        case 10.1...20:
            iH = roomHeight
            while iH != roomHeight  {
                iH += 0.1
            }
            Hmultiplier = 300 / iH
        case 20.1...30:
            iH = roomHeight
            while iH != roomHeight  {
                iH += 0.1
            }
            Hmultiplier = 300 / iH
        case 30.1...40:
            iH = roomHeight
            while iH != roomHeight  {
                iH += 0.1
            }
            Hmultiplier = 300 / iH
        case 40.1...50:
            iH = roomHeight
            while iH != roomHeight  {
                iH += 0.1
            }
            Hmultiplier = 300 / iH
        default:
            print("def")
        }
        
        print(Wmultiplier)
        print(Hmultiplier)
        if Wmultiplier < Hmultiplier{
            multiplier = Wmultiplier
        }else{
            multiplier = Hmultiplier
        }
        skView.frame = CGRect(x: widhtMainView/2-roomWidht*Wmultiplier/2, y: heightMainView/2-roomHeight*Hmultiplier/2, width: roomHeight*multiplier, height: roomWidht*multiplier)
        
    }
    func roomModel(skView: SKView) {
        skView.showsFPS = true
        skView.showsNodeCount = true
        skView.ignoresSiblingOrder = true
        
        
//        var Circle = SKShapeNode(circleOfRadius: 20)
//        Circle.position = CGPoint(x: 50, y: 100) // задаем позицию.
//        Circle.lineWidth = 10 // задаем размер линий.
//        Circle.strokeColor = SKColor.blue // задаем цвет контура.
//        Circle.fillColor = SKColor.red // задаем цвет внутренности.
//        Circle.name = "Circle" // задаем имя.
//        skView.scene?.addChild(Circle)
        //        self.addChild(Circle)
    }
    
}
