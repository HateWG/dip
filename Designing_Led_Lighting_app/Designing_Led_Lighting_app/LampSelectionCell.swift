//
//  LampSelectionCell.swift
//  Designing_Led_Lighting_app
//
//  Created by Томас Димеджи Акинделе Ало on 19/03/2019.
//  Copyright © 2019 Томас Димеджи Акинделе Ало. All rights reserved.
//

import UIKit

class LampSelectionCell: UITableViewCell, UITableViewDelegate {

    @IBOutlet weak var lampModelLabel: UILabel!
    
    var model: String = "1" {
        didSet {
            lampModelLabel.text = model
        }
    }
    
    @IBOutlet weak var lampModelImage: UIImageView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
