//
//  DesignableSlider.swift
//  Designing_Led_Lighting_app
//
//  Created by Томас Димеджи Акинделе Ало on 13/03/2019.
//  Copyright © 2019 Томас Димеджи Акинделе Ало. All rights reserved.
//

import UIKit

class DesignableSlider: UISlider {

    @IBInspectable var thumbImage: UIImage? {
        didSet {
            setThumbImage(thumbImage, for: .normal)
        }
    }
    
    @IBInspectable var thumbHighlightedImage: UIImage? {
        didSet {
            setThumbImage(thumbHighlightedImage, for: .highlighted)
        }
    }
}
