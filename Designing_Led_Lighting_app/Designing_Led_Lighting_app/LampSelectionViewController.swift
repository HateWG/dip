//
//  LampSelectionViewController.swift
//  Designing_Led_Lighting_app
//
//  Created by Томас Димеджи Акинделе Ало on 15/03/2019.
//  Copyright © 2019 Томас Димеджи Акинделе Ало. All rights reserved.
//

import UIKit

class LampSelectionViewController: UIViewController {
    @IBOutlet weak var lightOutputFrom: UITextField!
    @IBOutlet weak var lightOutputTo: UITextField!
    
    @IBOutlet weak var powerFrom: UITextField!
    @IBOutlet weak var powerTo: UITextField!

    @IBOutlet weak var dimensionsButton: UIButton!
    
    var ModelController: ModelConroller!
    
    struct dimensionsStruct {
        var firxtValue: Int
        var secondValue: Int
    }
    var dimensionsValue = dimensionsStruct(firxtValue: 0, secondValue: 0)
    
//    ----------------------------------------
    @IBOutlet weak var dispersionTypeButton: UIButton!
    var dispersionType: String = " "
//    ---------------------------------------
    
    @IBOutlet weak var lightSourceColorButton: UIButton!
    var lightSourceColor: String = " "
//    ------------------------------------
    @IBOutlet weak var seriasButton: UIButton!
    var serias: String = " "
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func dimensionsButton(_ sender: Any) {
        let alertController = UIAlertController (title: nil, message: "choose", preferredStyle: .actionSheet)
        
        
        let actionNill = UIAlertAction(title: "Не задано", style: .default) { (action) in
            self.dimensionsButton.setTitle("Не задано", for: .normal)
            self.dimensionsValue = dimensionsStruct(firxtValue: 0, secondValue: 0)
        }
        let action300x300 = UIAlertAction(title: "300x300", style: .default) { (action) in
            self.dimensionsButton.setTitle("300x300", for: .normal)
            self.dimensionsValue = dimensionsStruct(firxtValue: 300, secondValue: 300)
        }
        let action600x300 = UIAlertAction(title: "600x300", style: .default) { (action) in
            self.dimensionsButton.setTitle("600x300", for: .normal)
            self.dimensionsValue = dimensionsStruct(firxtValue: 600, secondValue: 300)
        }
        let action600x600 = UIAlertAction(title: "600x600", style: .default) { (action) in
            self.dimensionsButton.setTitle("600x600", for: .normal)
            self.dimensionsValue = dimensionsStruct(firxtValue: 600, secondValue: 600)
        }
        let action1200x300 = UIAlertAction(title: "1200x300", style: .default) { (action) in
            self.dimensionsButton.setTitle("1200x300", for: .normal)
            self.dimensionsValue = dimensionsStruct(firxtValue: 1200, secondValue: 300)
        }
        
        alertController.addAction(actionNill)
        alertController.addAction(action300x300)
        alertController.addAction(action600x300)
        alertController.addAction(action600x600)
        alertController.addAction(action1200x300)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func dispersionTypeButtonAction(_ sender: Any) {
        let alertController = UIAlertController (title: nil, message: "choose", preferredStyle: .actionSheet)
        
        
        let actionNill = UIAlertAction(title: "Не задано", style: .default) { (action) in
            self.dispersionTypeButton.setTitle("Не задано", for: .normal)
            self.dispersionType = "Не задано"
        }
        let actionCrushedIce = UIAlertAction(title: "Колотый лед", style: .default) { (action) in
            self.dispersionTypeButton.setTitle("Колотый лед", for: .normal)
            self.dispersionType = "Колотый лед"
        }
        let actionLactic = UIAlertAction(title: "Молочный", style: .default) { (action) in
            self.dispersionTypeButton.setTitle("Молочный", for: .normal)
            self.dispersionType = "Молочный"
        }
        let actionPrismatic = UIAlertAction(title: "Призматический", style: .default) { (action) in
            self.dispersionTypeButton.setTitle("Призматический", for: .normal)
            self.dispersionType = "Призматический"
        }
        alertController.addAction(actionNill)
        alertController.addAction(actionCrushedIce)
        alertController.addAction(actionLactic)
        alertController.addAction(actionPrismatic)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func getDispersionValue(dispersionType: String) -> Int{
        var value: Int = 0
        switch dispersionType {
        case "Не задано":
            value = 0
        case "Колотый лед":
            value = 1
        case "Молочный":
            value = 2
        case "Призматический":
            value = 3
        default:
            print("Error of func getDispersionValue")
        }
        return value
    }
    
    @IBAction func lightSourceColorButtonAction(_ sender: Any) {
        
        let alertController = UIAlertController (title: nil, message: "choose", preferredStyle: .actionSheet)
        
        
        let actionNill = UIAlertAction(title: "Не задано", style: .default) { (action) in
            self.lightSourceColorButton.setTitle("Не задано", for: .normal)
            self.lightSourceColor = "Не задано"
        }
        let actionWarmWhite = UIAlertAction(title: "Тепло-белый", style: .default) { (action) in
            self.lightSourceColorButton.setTitle("Тепло-белый", for: .normal)
            self.lightSourceColor = "warmWhite"
        }
        let actionNeutralWhite = UIAlertAction(title: "Нейтрально-белый", style: .default) { (action) in
            self.lightSourceColorButton.setTitle("Нейтрально-белый", for: .normal)
            self.lightSourceColor = "neutralWhite"
        }
        let actionColdWhite = UIAlertAction(title: "Холодно-белый", style: .default) { (action) in
            self.lightSourceColorButton.setTitle("Холодно-белый", for: .normal)
            self.lightSourceColor = "coldWhite"
        }
        alertController.addAction(actionNill)
        alertController.addAction(actionWarmWhite)
        alertController.addAction(actionNeutralWhite)
        alertController.addAction(actionColdWhite)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func serieaButtonAction(_ sender: Any) {
        let alertController = UIAlertController (title: nil, message: "choose", preferredStyle: .actionSheet)
        
        
        let actionNill = UIAlertAction(title: "Не задано", style: .default) { (action) in
            self.seriasButton.setTitle("Не задано", for: .normal)
            self.serias = "Не задано"
        }
        let actionStandart = UIAlertAction(title: "Standart", style: .default) { (action) in
            self.seriasButton.setTitle("Standart", for: .normal)
            self.serias = "standart"
        }
        let actionAdvanced = UIAlertAction(title: "Advanced", style: .default) { (action) in
            self.seriasButton.setTitle("Advanced", for: .normal)
            self.serias = "advanced"
        }
        alertController.addAction(actionNill)
        alertController.addAction(actionStandart)
        alertController.addAction(actionAdvanced)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "LampSendSegue" {
            if let controller = segue.destination as? LampSelectionTableView {
                controller.ModelController = ModelController
                
//                Световой поток от ... до
                controller.lightOutputFrom = (lightOutputFrom.text! as NSString).doubleValue
                controller.lightOutputTo = (lightOutputTo.text! as NSString).doubleValue
                
//                Мощность от ... до
                controller.powerFrom = (powerFrom.text! as NSString).doubleValue
                controller.powerTo = (powerTo.text! as NSString).doubleValue
                
//                Габаритные размеры
                controller.firstDimensionValue = dimensionsValue.firxtValue
                controller.secondDimensionValue = dimensionsValue.secondValue
                
//                Тип рассеивателя
                controller.dispersionValue = getDispersionValue(dispersionType: dispersionType)
                
//                Цвет источника света
                controller.lightSourceColor = lightSourceColor
                
//                Серия
                controller.seriasValue = serias
                
//                controller.luminaireOrientation = luminaireOrientation.selectedSegmentIndex
//                
//                controller.requiredIllumination = Int((requiredIllumination.text! as NSString).intValue)
            }
            
        }
    }
    
    
}
